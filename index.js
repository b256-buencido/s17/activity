// console.log("Hello World")

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	

	//first function here:
	function userDetails() {

		let userName = prompt("What is your name?");


		console.log("Hello, " + userName);

		let userAge = prompt("How old are you?");

		console.log("You are " + userAge + " years old.");

		let userLocation = prompt("Where do you live?");

		console.log("You live in " + userLocation + ".");

		alert("Thank you for your input!")


	}

	userDetails();
	

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favoriteArtist() {

		let artist1 = "The Beatles";
		let artist2 = "Metallica";
		let artist3 = "The Eagles";
		let artist4 = "Mozart";
		let artist5 = "Beethoven";

		console.log("1. " + artist1);
		console.log("2. " + artist2);
		console.log("3. " + artist3);
		console.log("4. " + artist4);
		console.log("5. " + artist5);
	}

	favoriteArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favoriteMovie() {

		let movie1 = "The Imitation Game";
		let rating1 = "Rotten Tomatoes Rating: 90%"

		let movie2 = "The Founder";
		let rating2 = "Rotten Tomatoes Rating: 81%"

		let movie3 = "The Pursuit of Happyness";
		let rating3 = "Rotten Tomatoes Rating: 67%"

		let movie4 = "The Wolf of Wallstreet";
		let rating4 = "Rotten Tomatoes Rating: 80%"

		let movie5 = "The Social Network";
		let rating5 = "Rotten Tomatoes Rating: 96%"

		console.log("1. " + movie1);
		console.log(rating1)

		console.log("2. " + movie2);
		console.log(rating2)

		console.log("3. " + movie3);
		console.log(rating3)

		console.log("4. " + movie4);
		console.log(rating4)

		console.log("5. " + movie5);
		console.log(rating5)
	}

	favoriteMovie();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};



// console.log(friend1);
// console.log(friend2);

printFriends();